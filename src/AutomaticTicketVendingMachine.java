import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutomaticTicketVendingMachine {
    private JPanel root;
    private JLabel topLavel;
    private JButton carbonaraButton;
    private JButton bologneseButton;
    private JButton genoveseButton;
    private JButton neapolitanButton;
    private JButton pomodoroButton;
    private JButton arrabiataButton;
    private JTextPane orderForm;
    private JButton checkOutButton;
    private JLabel total;
    private JButton cancelButton;

    int sum = 0;

    void order(String food, int price) {

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            int confirmation2 = JOptionPane.showConfirmDialog(
                    null,
                    "How about a salad with " + food + "?(+200yen)",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            int confirmation3 = JOptionPane.showConfirmDialog(
                    null,
                    "How about soup with " + food + "?(+100yen)",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! \nIt'll be served as soon as possible.");
            String currentText = orderForm.getText();
            orderForm.setText(currentText + " " + food + " (" + price + "yen)\n");
            if (confirmation2 == 0){
                sum += 200;
                orderForm.setText(currentText + " " + food + "+ a salad (" + (price+200) + "yen)\n");
                if (confirmation3 == 0){
                    sum += 100;
                    orderForm.setText(currentText + " " + food + " + a salad and soup (" + (price+300) + "yen)\n");
                }
            }
            else if (confirmation3 == 0){
                sum += 100;
                orderForm.setText(currentText + " " + food + " + soup (" + (price+100) + "yen)\n");
            }
            sum += price;
            total.setText("Total    " + sum + " yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("AutomaticTicketVendingMachine");
        frame.setContentPane(new AutomaticTicketVendingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public AutomaticTicketVendingMachine() {
         carbonaraButton.setIcon(new ImageIcon(this.getClass().getResource("carbonara.jpg")));
         bologneseButton.setIcon(new ImageIcon(this.getClass().getResource("bolognese.jpg")));
         genoveseButton.setIcon(new ImageIcon(this.getClass().getResource("genovese.jpg")));
         neapolitanButton.setIcon(new ImageIcon(this.getClass().getResource("neapolitan.jpg")));
         pomodoroButton.setIcon(new ImageIcon(this.getClass().getResource("pomodoro.jpg")));
         arrabiataButton.setIcon(new ImageIcon(this.getClass().getResource("arrabiata.jpg")));
            carbonaraButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e){
                    order("Carbonara", 880);
                }
            });
            bologneseButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Bolognese", 660);
                }
            });
            genoveseButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Genovese", 700);
                }
            });
            neapolitanButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Neapolitan", 720);
                }
            });
            pomodoroButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Pomodoro", 690);
                }
            });
            arrabiataButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Arrabiata", 770);
                }
            });
            checkOutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int confirmation = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Thank you! The total price is " + sum + " yen.");
                        sum = 0;
                        total.setText("Total    " + sum + " yen");
                        orderForm.setText("");
                    }
                }
            });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    sum = 0;
                    total.setText("Total    " + sum + " yen");
                    orderForm.setText("");
                }
            }
        });
    }
    }
